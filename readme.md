# Firebase usage

1. You must have the Firebase CLI installed. If you don't have it install it with npm install -g firebase-tools and then configure it with firebase login.
2. On the command line run firebase use --add and select the Firebase project you have created.
3. On the command line run firebase serve using the Firebase CLI tool to launch a local server.

# Create a new User
```
function newUser() {
  var email = <Get email from field>;
  var password = <Get password from field>;
  firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;
    if (errorCode == 'auth/weak-password') {
      alert('The password is too weak.');
    } else {
      alert(errorMessage);
    }
    console.log(error);
  });
}

```

# Login a known User
```
    var email = <Get email from field>;
    var password = <Get password from field>;

    firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // [START_EXCLUDE]
    if (errorCode === 'auth/wrong-password') {
        alert('Wrong password.');
    } else {
        alert(errorMessage);
    }
        console.log(error);
    });
```

# Add a users information to the database
```
function pushdata() {
    var uid = firebase.auth().currentUser.uid;
    var userObject = {
        uid : uid,
        fullname: <Get from field: pass as string>,
        courses : {
            //Begins empty, will add functionn to add courses soon
        },
        isInstructor: <boolean: true || false>
    }
    var updates = {};
    updates['/users/' + uid] = userObject;
    return firebase.database().ref().update(updates);
}
```

# Create a new Course
```
function pushNewCourse() {
  var courseID = firebase.database().ref().child('courses').push().key;
  console.log(courseID);
  var courseObject = {
      id : courseID,
      instructor: <Instructors UID>,
      title: <Course Title>,
      students: {
        //Starts empty
      },
      quizzes: {
        //Starts empty
      },
      userQuizResponses: {
        //Starts empty
      },
      lectureQuizResponses: {
        //Starts empty
      },
      announcements: {
        //Starts empty
      },
      lectures: {
        //Starts empty
      }
  }
  var updates = {};
  updates['/courses/' + courseID ] = courseObject;
  return firebase.database().ref().update(updates);
}
```
# Add Course to User
```
function addCourseToUser(courseID){
  uid = firebase.auth().currentUser.uid;
  // A known bug is that courses can be pushed to the same user more than once
  firebase.database().ref().child('users').child(uid).child('courses').push(courseID);
  return;
}
```



# Get Users courses
```
function getUsersCourses(){
  uid = firebase.auth().currentUser.uid;
  firebase.database().ref('/users/' + uid + '/courses').once('value').then(function(snapshot) {
      //prints a json of courses to the console
      //Course id is the value, the keys are meaning less
      console.log(snapshot.val());
  });
}
```

# Add lectures to a Course
```
function pushLecture(cid) {
  console.log(cid);
  var lecturesID = firebase.database().ref().child('courses').child(cid).child('lectures').push().key;
  console.log(lecturesID);
  var lectureObject = {
    "description" : "Grep VS AWK",
    "id" : lecturesID,
    "instructor" : "Rodriguez-Rivera",
    "lectnum" : 5,
    "length" : "35m 14s",
    "postTime" : "Friday, Febuary 12, 2018 6:21:46 PM EST"
  }
  var updates = {};
  updates['/courses/' + cid + '/lectures/' + lecturesID] = lectureObject;
  return firebase.database().ref().update(updates);
}
```